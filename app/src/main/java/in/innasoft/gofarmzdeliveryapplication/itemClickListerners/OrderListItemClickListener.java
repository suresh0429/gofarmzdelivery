package in.innasoft.gofarmzdeliveryapplication.itemClickListerners;

import android.view.View;

public interface OrderListItemClickListener
{
    void onItemClick(View v, int pos);
}
