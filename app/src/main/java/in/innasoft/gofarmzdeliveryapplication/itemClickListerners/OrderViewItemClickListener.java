package in.innasoft.gofarmzdeliveryapplication.itemClickListerners;

import android.view.View;

public interface OrderViewItemClickListener
{
    void onItemClick(View v, int pos);
}
