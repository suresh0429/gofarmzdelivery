package `in`.innasoft.gofarmzdeliveryapplication.activity


import `in`.innasoft.gofarmzdeliveryapplication.R
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.view.View
import com.akexorcist.googledirection.GoogleDirection
import com.akexorcist.googledirection.config.GoogleDirectionConfiguration
import com.akexorcist.googledirection.constant.TransportMode
import com.akexorcist.googledirection.model.Direction
import com.akexorcist.googledirection.model.Route
import com.akexorcist.googledirection.util.DirectionConverter
import com.akexorcist.googledirection.util.execute
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_waypoints_direction.*
import java.util.*

class WaypointsDirectionActivity : AppCompatActivity() {

    companion object {
        // private const val serverKey = "AIzaSyCWNkrF-2U0Jc48DvvtydZ5Kv-gLTDIaOk"
        // private const val serverKey = "AIzaSyB6Moh3QdGilMNz5ubXHIRy08Vg5X_8SWg"
        private const val serverKey = "AIzaSyBsgAoNL5svUOXLQPK34MgclILNCrRFvvs"
       // private val park = LatLng(17.512510, 78.352226)
        private var park : LatLng? = null
        private var sourcelatitude : Double = 0.00
        private var sourcelongitude : Double = 0.00
        private val shopping = LatLng(41.8766061, -87.6556908)
        private val dinner = LatLng(41.8909056, -87.6467561)
        private val gallery = LatLng(17.461100, 78.536072)
        var waypoints= arrayListOf<LatLng>()
    }

    private var googleMap: GoogleMap? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_waypoints_direction)

        if(intent != null ){
             sourcelatitude = intent.getDoubleExtra("latitude",0.0)
             sourcelongitude = intent.getDoubleExtra("longitude",0.0)
             park = LatLng(17.512510, 78.352226)

        }

        buttonRequestDirection.setOnClickListener { requestDirection() }

        (supportFragmentManager.findFragmentById(R.id.maps) as SupportMapFragment).getMapAsync { googleMap ->
            this.googleMap = googleMap
        }
    }

    private fun requestDirection() {
        val arrayList = ArrayList<LatLng>()
        arrayList.add(LatLng(17.494793, 78.399643))
        arrayList.add(LatLng(17.466681, 78.395180))
        arrayList.add(LatLng(17.437462, 78.448288))


        for(i in arrayList){
            waypoints.add(i)
        }
        /* var waypoints = Arrays.asList(
                 LatLng(41.8766061, -87.6556908),
                 LatLng(41.8909056, -87.6467561),
                 LatLng(41.8909076, -87.6467571))*/

        showSnackbar(getString(R.string.direction_requesting))
        GoogleDirectionConfiguration.getInstance().isLogEnabled = true
        GoogleDirection.withServerKey(serverKey)
                .from(park)
                .and(waypoints)
                .to(gallery)
                .transportMode(TransportMode.DRIVING)
                .execute(
                        onDirectionSuccess = { direction -> onDirectionSuccess(direction) },
                        onDirectionFailure = { t -> onDirectionFailure(t) }
                )
    }

    private fun onDirectionSuccess(direction: Direction) {
        showSnackbar(getString(R.string.success_with_status, direction.status))
        if (direction.isOK) {
            val route = direction.routeList[0]
            val legCount = route.legList.size
            for (index in 0 until legCount) {
                val leg = route.legList[index]
                googleMap?.addMarker(MarkerOptions().position(leg.startLocation.coordination))
                if (index == legCount - 1) {
                    googleMap?.addMarker(MarkerOptions().position(leg.endLocation.coordination))
                }
                val stepList = leg.stepList
                val polylineOptionList = DirectionConverter.createTransitPolyline(this, stepList, 5, Color.RED, 3, Color.BLUE)
                for (polylineOption in polylineOptionList) {
                    googleMap?.addPolyline(polylineOption)
                }
            }
            setCameraWithCoordinationBounds(route)
            buttonRequestDirection.visibility = View.GONE
        } else {
            showSnackbar(direction.status)
            Log.e("TAG",""+direction.status)
        }
    }

    private fun onDirectionFailure(t: Throwable) {
        showSnackbar(t.message)

    }

    private fun setCameraWithCoordinationBounds(route: Route) {
        val southwest = route.bound.southwestCoordination.coordination
        val northeast = route.bound.northeastCoordination.coordination
        val bounds = LatLngBounds(southwest, northeast)
        googleMap?.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100))
    }

    private fun showSnackbar(message: String?) {
        message?.let {
            Snackbar.make(findViewById(android.R.id.content), message.toString(), Snackbar.LENGTH_SHORT).show()
        }
    }
}
