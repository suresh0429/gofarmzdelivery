package in.innasoft.gofarmzdeliveryapplication.activity;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;


import com.google.android.material.snackbar.Snackbar;

import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import in.innasoft.gofarmzdeliveryapplication.Connectivity;
import in.innasoft.gofarmzdeliveryapplication.GPSTracker;
import in.innasoft.gofarmzdeliveryapplication.Globals;
import in.innasoft.gofarmzdeliveryapplication.R;
import in.innasoft.gofarmzdeliveryapplication.adapters.OrdersAdapter;
import in.innasoft.gofarmzdeliveryapplication.database.OrdersDbHelper;
import in.innasoft.gofarmzdeliveryapplication.model.Orders;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, LocationListener {
    private LocationManager locationManager;
    private String provider;
    TextView name_txt, filter_txt,route_txt;
    RecyclerView recyclerViewOrders;
   // GPSTracker gpsTracker;
    ProgressDialog progressDialog;
    int filter_distance = 0;
    public static String currency;
    ArrayList<Orders> ordersArrayList;
    OrdersAdapter adapter;
    Dialog dialog;
    OrdersDbHelper ordersDbHelper;
    Button delivered_btn;

    LinearLayoutManager layoutManager;
    int total_number_of_items = 0;
    private boolean userScrolled = true;
    private static int displayedposition = 0;
    int type_of_request = 0;
    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    int defaultPageNo = 1;
    Double latitude,longitude;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

     /*   gpsTracker = new GPSTracker(MainActivity.this);
        Log.d("TAG", "onCreate: " + gpsTracker.getLatitude() + "___" + gpsTracker.getLongitude());
*/

        // Get the location manager
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        // Define the criteria how to select the locatioin provider -> use
        // default
        Criteria criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    Activity#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for Activity#requestPermissions for more details.
                return;
            }
        }
        Location location = locationManager.getLastKnownLocation(provider);

        // Initialize the location fields
        if (location != null) {
            System.out.println("Provider " + provider + " has been selected.");
            onLocationChanged(location);
        } else {


            Log.d("TAG", "onCreate: " + "Location not available");
        }


        ordersDbHelper = new OrdersDbHelper(this);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        name_txt = findViewById(R.id.name_txt);
        filter_txt = findViewById(R.id.filter_txt);
        route_txt = findViewById(R.id.route_txt);
        route_txt.setOnClickListener(this);
        ordersArrayList = new ArrayList<>();

        delivered_btn = findViewById(R.id.delivered_btn);
        delivered_btn.setOnClickListener(this);


        recyclerViewOrders = findViewById(R.id.recyclerViewOrders);
        recyclerViewOrders.setHasFixedSize(true);
        adapter = new OrdersAdapter(MainActivity.this, ordersArrayList, R.layout.row_orders);
        layoutManager = new LinearLayoutManager(MainActivity.this);
        recyclerViewOrders.setLayoutManager(layoutManager);
        RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecorator(ContextCompat.getDrawable(MainActivity.this, R.drawable.divider));
        recyclerViewOrders.addItemDecoration(dividerItemDecoration);
        recyclerViewOrders.setNestedScrollingEnabled(false);


        if (Globals.user != null) {
            name_txt.setText(Html.fromHtml("Welcome " + "<b>" + Globals.user.name + "</b> "));
        }

//        if (Connectivity.isConnected(MainActivity.this)) {
//            getOrders();
//        } else {
//            Snackbar snackbar = Snackbar.make(MainActivity.this.getWindow().getDecorView().getRootView(), "No Internet Connection...!", Snackbar.LENGTH_LONG);
//            snackbar.show();
//        }

        filter_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog = new Dialog(MainActivity.this, R.style.Theme_AppCompat_DayNight_Dialog_Alert);
                dialog.setContentView(R.layout.filter_dialog);
                dialog.setTitle("Filter");
                dialog.setCancelable(true);
                dialog.show();

                final SeekBar seekbar = dialog.findViewById(R.id.seekBar);
                final TextView textDistance = dialog.findViewById(R.id.seekDistance);
                Button buttonSubmit = dialog.findViewById(R.id.buttonSubmit);

                seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                        textDistance.setText("" + seekBar.getProgress() + " km");
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                });

                buttonSubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        dialog.dismiss();

                        filter_distance = seekbar.getProgress();
                        if (Connectivity.isConnected(MainActivity.this)) {
                            getOrders();
                        } else {
                            Snackbar snackbar = Snackbar.make(MainActivity.this.getWindow().getDecorView().getRootView(), "No Internet Connection...!", Snackbar.LENGTH_LONG);
                            snackbar.show();
                            saveToDB();
                        }
                    }
                });
            }
        });


        recyclerViewOrders.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();
                    displayedposition = pastVisiblesItems;

                    if (loading) {
                        if (userScrolled && (visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            userScrolled = false;
                            defaultPageNo = defaultPageNo + 1;
                            if (totalItemCount < total_number_of_items) {
                                type_of_request = 1;

                                adapter.notifyDataSetChanged();
                                getOrders();
                            } else {
                                Toast.makeText(MainActivity.this, "No More Results", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    userScrolled = true;
                }
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (Connectivity.isConnected(MainActivity.this)) {
            getOrders();
        } else {
            Snackbar snackbar = Snackbar.make(MainActivity.this.getWindow().getDecorView().getRootView(), "No Internet Connection...!", Snackbar.LENGTH_LONG);
            snackbar.show();
            saveToDB();
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    Activity#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for Activity#requestPermissions for more details.
                return;
            }
        }
        locationManager.requestLocationUpdates(provider, 400, 1, this);
    }

    private void getOrders()
    {
        if (Globals.user != null)
        {
            progressDialog.show();
            ordersArrayList.clear();
            HashMap<String, String> headers = new HashMap<>();
            HashMap<String, String> params = new HashMap<>();

            String data = "user_id=" + Globals.user.userId + "&page_no="+defaultPageNo+"&filter_distance=" + filter_distance + "" +
                    "&latitude=" + latitude + "&longitude=" + longitude;

            Log.v("URLLLL", Globals.getBaseURL() + "orders?" + data);
            Globals.GET(Globals.getBaseURL() + "orders?" + data, headers, params, new Globals.VolleyCallback() {
                @Override
                public void onSuccess(String result) {
                    Log.v("resultMain success", result);
                    progressDialog.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("status").equalsIgnoreCase("10100"))
                        {

                            JSONObject object = jsonObject.getJSONObject("data");

                            if (object.has("currency") || !object.isNull("currency"))
                                currency = object.getString("currency");

                            int total_numberof_records = Integer.valueOf(object.getString("recordTotalCnt"));
                            if (total_numberof_records == 0) {
                                Toast.makeText(MainActivity.this, "No Data Found", Toast.LENGTH_SHORT).show();
                            }
                            total_number_of_items = total_numberof_records;
                            if (total_numberof_records > ordersArrayList.size()) {
                                loading = true;
                            } else {
                                loading = false;
                            }

                            JSONArray jsonArray = object.getJSONArray("recordData");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                Orders order = new Orders(jsonArray.getJSONObject(i));
                                ordersArrayList.add(order);
                            }

                           recyclerViewOrders.setAdapter(adapter);
                            adapter.notifyDataSetChanged();

                            saveToDB();


                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFail(String result) {
                    Log.v("resultMain fail ", result);
                    progressDialog.dismiss();
                }
            });
        }
    }

    private void saveToDB() {


        if (Connectivity.isConnected(MainActivity.this)) {

            ordersDbHelper.deleteOrderList();

            for (int i = 0; i < ordersArrayList.size(); i++) {

                ContentValues values = new ContentValues();
                values.put("orderId", ordersArrayList.get(i).referenceId);
                values.put("name", ordersArrayList.get(i).name);
                values.put("location", ordersArrayList.get(i).area);
                values.put("price", ordersArrayList.get(i).finalPrice);
                values.put("gatewayName", ordersArrayList.get(i).getawayName);
                values.put("status", ordersArrayList.get(i).status);
                values.put("mobilenumber", ordersArrayList.get(i).mobile);
                ordersDbHelper.addOrderList(values);
            }



        } else {

            ordersArrayList.clear();

            List<String> orderIdList = ordersDbHelper.getOrderId();
            List<String> nameList = ordersDbHelper.getName();
            List<String> locationList = ordersDbHelper.getLocation();
            List<String> priceList = ordersDbHelper.getPrice();
            List<String> gatewayList = ordersDbHelper.getGateway();
            List<String> statusList = ordersDbHelper.getStatus();
            List<String> mobileNumberList = ordersDbHelper.getMobilenumber();

            for (int i = 0; i < nameList.size() ; i++) {
                Orders orders = new Orders();
                orders.referenceId = orderIdList.get(i);
                orders.name = nameList.get(i);
                orders.area = locationList.get(i);
                orders.finalPrice = priceList.get(i);
                orders.getawayName = gatewayList.get(i);
                orders.status = statusList.get(i);
                orders.mobile = mobileNumberList.get(i);

                ordersArrayList.add(orders);

            }

            recyclerViewOrders.setAdapter(adapter);

            ordersDbHelper.close();

        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (dialog != null)
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
    }

    @Override
    public void onClick(View view) {

        if(view==delivered_btn)
        {
            Intent next=new Intent(MainActivity.this,OrderDeliveredListActivity.class);
            startActivity(next);
        }

        if(view==route_txt)
        {
            Intent next=new Intent(MainActivity.this,WaypointsDirectionActivity.class);
            next.putExtra("latitude",latitude);
            next.putExtra("longitude",longitude);
            startActivity(next);
        }
    }



    /* Remove the locationlistener updates when Activity is paused */
    @Override
    protected void onPause() {
        super.onPause();
        locationManager.removeUpdates(this);
    }

    @Override
    public void onLocationChanged(Location location) {
        int lat = (int) (location.getLatitude());
        int lng = (int) (location.getLongitude());

         latitude = location.getLatitude();
         longitude = location.getLongitude();

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onProviderEnabled(String provider) {
        Toast.makeText(this, "Enabled new provider " + provider, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(this, "Disabled provider " + provider, Toast.LENGTH_SHORT).show();
    }

}
